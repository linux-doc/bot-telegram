'use strict';

const express = require('express');
const axios = require('axios');

// На будущее. Для валидации и зачистки входящей информации.
// https://express-validator.github.io/docs/index.html

const BOT_INTERFACE = process.env.BOT_INTERFACE || '127.0.0.1';
const BOT_PORT = process.env.BOT_PORT || 3000;
// app.set('port', process.env.BOT_PORT || 3000);

const app = express();

app.use(express.json());                          // to support JSON-encoded bodies
app.use(express.urlencoded({ extended: true }));  // to support URL-encoded bodies

const TELEGRAM_BOT_TOKEN = process.env.TELEGRAM_BOT_TOKEN;

///// Блок функций для отправки /////

let send = (method, data) => {
  axios.post(`https://api.telegram.org/bot${TELEGRAM_BOT_TOKEN}/${method}`, data)
  .then(function (response) {
    console.log(response.data);
  })
  .catch(function (error) {
    console.log(error.data);
  });
}

let sendMessage = data => {
  send(
    'sendMessage',
    data
  );
}

let sendSticker = data => {
  send(
    'sendSticker',
    data
  );
}

let getMenu = (body) => {
  sendMessage({
    chat_id: body.message ? body.message.chat.id : body.callback_query.message.chat.id,
    text: "Выберите действие:",
    reply_markup: {
      inline_keyboard: [
        [{text: 'Получить информацию о проекте', callback_data: 'getInfo'}],
        [{text: 'Перейти на сайт Linux Doc', url: 'https://linux-doc.ru/'}]
      ]
    }
  });
}

let getInfo = (body) => {
  sendMessage({
    chat_id: body.message ? body.message.chat.id : body.callback_query.message.chat.id,
    text: "Проект по созданию веб-платформы для свободного общения и продвижения дистрибутивов на базе ядра Linux.",
  });
}

///// Маршруты /////

app.get('/', (req, res) => {
  console.log(req.params);
  res.send('Hello World');
});

app.post('/', (req, res) => {
  console.log('/ параметры');
  console.log(req.params);
  // console.log(req.body);
  res.send('Hello World');
});

// Обработчик GET нужен только для проверки доступности пути /webhook.
app.get('/webhook', (req, res) => {
  console.log('/webhook параметры запроса');
  console.log(req.query);

  res.type('text/plain');
  res.send('Путь /webhook доступен');
});

// Сервер Телеграм присылает всё через POST.
// В req.params попадает то, что записывается через ":", например, /webhook/:test_path, добавит test_path.
// В req.query попадает то, что идёт в части запроса в строке URL после "?"
// В req.body попадает то, что передаётся в теле POST-запроса
app.post('/webhook', (req, res) => {
  console.log('WH POST');
  console.log(req.body);

  if (req.body.message) {
    let chat_id = req.body.message.chat.id;
    // if (
    //   chat_id !== -799987486      // чат разработчиков Водоноса
    //   && chat_id !== -752838370   // чат Water gangbang
    //   && chat_id !== 283943071    // чат Александра Туманова
    // ) {
    //   sendMessage({
    //     chat_id: chat_id,
    //     text: 'Бот находится в стадии разработки. По всем вопросам о боте вы можете обратиться к @justice1980',
    //     reply_markup: {
    //       inline_keyboard: [
    //         [{text: 'Перейти на сайт Водоноса', url: 'https://vodonos.roboto.pro/'}]
    //       ]
    //     }
    //   });

    //   res.send('Hello World');
    //   return;
    // }

    // Если в строке текста передавали какие-нибудь команды, то они попадают в массив entities
    if (
      req.body.message.entities
      && req.body.message.entities.length !== 0
    ) {
      // В массив req.body.message.entities попадают все строки, которые сервер интерпретирует,
      // как один из специальных типов:
      // mention - упоминание пользователя (@username)
      // hashtag - хештег (#hashtag)
      // cashtag - кештег или код валюты ($USD)
      // bot_command - команда для бота (/menu@vodonos_roboto_pro_bot или просто /menu)
      // url - URL (https://vodonos.roboto.pro/)
      // email - адрес электронной почты (vodonos@roboto.pro)
      // phone_number - номер телефона (+7(987)123-4567)
      // bold - жирный шрифт
      // italic - наклонный шрифт
      // underline - подчёркнутый текст
      // strikethrough - зачёркнутый текст
      // spoiler - spoiler message (замазанное сообщение)
      // code - моноширинная строка текста (форматирование через меню клиента)
      // pre - моноширинный блок текста (```)
      // text_link - для ссылок, у которых не виден URL (форматирование через меню клиента)
      // text_mention - имя пользователя, которого не существует.
      let entities = req.body.message.entities;
      console.log(req.body.message.entities);

      for (let i = 0; i < req.body.message.entities.length; i++) {
        console.log(i + ' ' + entities[i].type);
        if (entities[i].type === 'bot_command') {
          console.log('Запись: ' + i);
          console.log(entities[i]);
          console.log('offset: ' + entities[i].offset);
          console.log('length: ' + entities[i]['length']);
          console.log('Строка: ' + req.body.message.text);
          let bot_command = req.body.message.text.substring(entities[i].offset, entities[i].offset + entities[i].length);
          console.log('bot_command: ' + bot_command);
// Список команд для @BotFather
// get_my_id - Показать ID пользователя
// get_chat_id - Показать ID чата
// get_menu - Меню бота
          // Если серверу Телеграм ничего не возвращать, а выходить из функции с помощью return, то
          // сервер Телеграм будет считать, что его сообщение НЕ доставлено и будет пытаться отправить
          // его повторно каждые две минуты.
          switch(bot_command) {
            // Стандартная команда запуска бота
            case '/start':
              // Ничего не делать
            break;

            // Показать ID пользователя Телеграм
            case '/get_my_id':
            case '/get_my_id@linux_doc_bot':
              console.log('КОМАНДА /get_my_id');

              let first_name = req.body.message.from.first_name;
              let last_name = req.body.message.from.last_name ? ' ' + req.body.message.from.last_name : '';
              sendMessage({
                chat_id: req.body.message.chat.id,
                text: `${first_name}${last_name}, ваш идентификатор: ${req.body.message.from.id}`
              });
            break;

            // Показать ID чата Телеграм
            case '/get_chat_id':
            case '/get_chat_id@linux_doc_bot':
              console.log('КОМАНДА /get_chat_id');
              
              sendMessage({
                chat_id: req.body.message.chat.id,
                text: `Идентификатор чата: ${req.body.message.chat.id}`
              });
            break;

            // Меню бота
            case '/get_menu':
            case '/get_menu@linux_doc_bot':
              console.log('Запрошено меню');
              getMenu(req.body);
            break;
      
            default:
              console.log('Команда ' + bot_command + ' не распознана');
              console.log(req.body.message.text);
              sendMessage({
                chat_id: req.body.message.chat.id,
                text: 'Команда ' + bot_command + ' не распознана'
              });

              // Отладка
              if (req.body.message.chat.id != 283943071) {
                sendMessage({
                  chat_id: "283943071",
                  text: 'Команда ' + bot_command + ' не распознана'
                });
              }
          }
        }
      }
    }
  }
  else if (req.body.callback_query) {
    let callback_query_id = req.body.callback_query.id;
    console.log('callback_query_id: ' + callback_query_id);
    let callback_query_data = req.body.callback_query.data;
    let command = callback_query_data.split('_')[0];
    switch (command) {
      case 'getInfo':
        getInfo(req.body);
      break;

      default:
        sendMessage({
          chat_id: "283943071",
          text: "Кнопка не распознана"
        });
    }

    axios.post(`https://api.telegram.org/bot${TELEGRAM_BOT_TOKEN}/answerCallbackQuery`, {
      callback_query_id: callback_query_id,
      text: req.body.callback_query.data
    })
    .then(function (response) {
      console.log('response');
    })
    .catch(function (error) {
      console.log('error');
    });
  }
  res.send('wh');
});

// пользовательская страница 404
app.use(function(req, res, next) {
  console.log('404, originalUrl:', req.originalUrl);
  res.type('text/plain');
  res.status(404);
  res.send('404 — Не найдено');
});

app.listen(BOT_PORT, BOT_INTERFACE);
console.log(`running on http://${BOT_INTERFACE}:${BOT_PORT}`);
