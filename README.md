# Запуск бота
```
pm2 start bot.js
```
## Инструкци для разработчика

### Сборка образа
```shell
docker build -t registry.gitlab.com/linux-doc/bot-telegram .
```

### Отправка образа в хранилище Gitlab
```shell
docker login registry.gitlab.com
docker push registry.gitlab.com/linux-doc/bot-telegram
```

### Запуск в Docker
```shell
read -p 'Введите ключ бота для Телеграм: ' TELEGRAM_BOT_TOKEN
export TELEGRAM_BOT_TOKEN
docker run \
     --name ld-bot \
     --env DEBUG=express \
     --env TELEGRAM_BOT_TOKEN=$TELEGRAM_BOT_TOKEN \
     --env BOT_INTERFACE='0.0.0.0' \
     --network ld-network \
     --publish 127.0.0.1:3000:3000 \
     --detach \
     registry.gitlab.com/linux-doc/bot-telegram
```

## Отправка запросов и сообщений в Телеграм
Оповещение водоносов через Телеграм

[Создать бота](https://core.telegram.org/bots#6-botfather)

Получить API-токен бота от @BotFather

Добавить бота в чат в который вы будете передавать сообщения

Зарегистрировать вебхук для бота
```
curl https://api.telegram.org/bot${TELEGRAM_BOT_TOKEN}/setWebhook?url=${WEBHOOK_URL}
```

Получить обновлённые данные для бота и посмотреть ID чата (отрицательное целое число):
```
curl https://api.telegram.org/bot${TELEGRAM_BOT_TOKEN}/getUpdates | jq
```
Отправить сообщение через HTTP API: https://core.telegram.org/bots/api#sendmessage
```
curl -X POST \
     -H 'Content-Type: application/json' \
     -d '{"chat_id": "123456789", "text": "This is a test from curl", "disable_notification": true}' \
     https://api.telegram.org/bot$TELEGRAM_BOT_TOKEN/sendMessage
```

## Полное описание API
https://core.telegram.org/bots/api

## Запуск всего проекта
```shell
docker-compose up -d
```
