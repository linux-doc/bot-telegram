FROM node:18-alpine
WORKDIR /usr/src/app
COPY . .
RUN npm install
# Если вы создаете сборку для продакшн
# RUN npm ci --only=production
EXPOSE 3000
CMD [ "node", "bot.js" ]
